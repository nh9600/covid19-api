package com.api.covid19.controller;

import java.io.IOException;

import javax.annotation.Resource;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

import com.api.covid19.service.Covid19Service;
import com.api.covid19.vo.Covid19VO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class Covid19Controller {
	
	@Resource
	private Covid19Service cs;
	
	
	//테스트용
	/*@GetMapping("/covid/result")
	public void covidResult(Covid19VO cvd) throws ParserConfigurationException, SAXException {
		try {
			cs.covidTest(cvd);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}*/
	
	@GetMapping("/covid/result1")
	public Covid19VO covidResult1(Covid19VO cvd) throws IOException, ParserConfigurationException {
		log.info("Covid Test => {}",cs.covidTest(cvd));
		String covid = "나는 마스터";
		return cs.covidTest(cvd);
	}
}
