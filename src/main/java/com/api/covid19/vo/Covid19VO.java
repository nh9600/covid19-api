package com.api.covid19.vo;

import lombok.Data;

@Data
public class Covid19VO {
	private String resultCode;
	private String resultMsg;

}
