package com.api.covid19.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.api.covid19.vo.Covid19VO;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class Covid19Service {
	
	@Value("${covid19.api.serviceKey}")
	private String serviceKey;
	@Resource
	private ObjectMapper om;
	
	//xml parsing
	public Covid19VO covidTest(Covid19VO cvd) throws IOException, ParserConfigurationException {
		String urlBuilder = "http://openapi.data.go.kr/openapi/service/rest/Covid19/getCovid19InfStateJson" /*URL*/
        +"?" + URLEncoder.encode("ServiceKey","UTF-8") + "=" + serviceKey /*공공데이터포털에서 받은 인증키*/
        +"&" + URLEncoder.encode("pageNo","UTF-8") + "=" + URLEncoder.encode("1", "UTF-8") /*페이지번호*/
        +"&" + URLEncoder.encode("numOfRows","UTF-8") + "=" + URLEncoder.encode("10", "UTF-8") /*한 페이지 결과 수*/
        +"&" + URLEncoder.encode("startCreateDt","UTF-8") + "=" + URLEncoder.encode("20200310", "UTF-8") /*검색할 생성일 범위의 시작*/
        +"&" + URLEncoder.encode("endCreateDt","UTF-8") + "=" + URLEncoder.encode("20200315", "UTF-8"); /*검색할 생성일 범위의 종료*/
		try {
		DocumentBuilderFactory dbFactoty = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactoty.newDocumentBuilder();
        Document doc = dBuilder.parse(urlBuilder);

        
        // root tag 
        System.out.println("Root element: " + doc.getDocumentElement().getNodeName()); // Root element: result
        
     	// 파싱할 tag
        NodeList nList = doc.getElementsByTagName("item");
        System.out.println("파싱할 리스트 수 : "+ nList.getLength());  // 파싱할 리스트 수 :  6
        
        for(int temp = 0; temp < nList.getLength(); temp++){	
        	Node nNode = nList.item(temp);
        	if(nNode.getNodeType() == Node.ELEMENT_NODE){
        						
        		Element eElement = (Element) nNode;
        		System.out.println("######################");
        		System.out.println("누적 확진률  : " + getTagValue("accDefRate", eElement));
        		cvd.setResultCode(getTagValue("accDefRate", eElement));
        		System.out.println("누적 검사 수  : " + getTagValue("accExamCnt", eElement));
        		cvd.setResultMsg(getTagValue("accExamCnt", eElement));
        		System.out.println(cvd);
        		log.info("나는 테스트다~=>{}",cvd);
        		//Covid19VO covid = om.readValue(cvd.toString(),Covid19VO.class);
                //log.info("test=>{}",covid);
                //System.out.println("나는 테스트~"+covid);
        		return cvd;
        	}	// for end
        	System.out.println("나는 테스트"+cvd);
        }	// if end
        System.out.println("나는 테스트"+cvd);
  
        URL url = new URL(urlBuilder.toString());
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-type", "application/json");
        System.out.println("Response code: " + conn.getResponseCode());
        
        BufferedReader rd;
        if(conn.getResponseCode() >= 200 && conn.getResponseCode() <= 300) {
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        } else {
            rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        }
        
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        rd.close();
        Covid19VO covid = om.readValue(cvd.toString(),Covid19VO.class);
        log.info("test=>{}",covid);
        System.out.println("나는 테스트~"+covid);
        //conn.disconnect();
        return cvd;
		} catch (SAXException e) {
			e.printStackTrace();
		}
		return cvd;
		
	}
	
	// xml parsing(tag값의 정보를 가져오는 메소드)
	private static String getTagValue(String tag, Element eElement) {
	    NodeList nlList = eElement.getElementsByTagName(tag).item(0).getChildNodes();
	    Node nValue = (Node) nlList.item(0);
	    if(nValue == null) 
	        return null;
	    return nValue.getNodeValue();
	}
	
	//json parsing
	public Covid19VO covidTest2(Covid19VO cvd) throws IOException {
		StringBuilder urlBuilder = new StringBuilder("http://openapi.data.go.kr/openapi/service/rest/Covid19/getCovid19InfStateJson"); /*URL*/
        urlBuilder.append("?" + URLEncoder.encode("ServiceKey","UTF-8") + "=" + serviceKey); /*공공데이터포털에서 받은 인증키*/
        urlBuilder.append("&" + URLEncoder.encode("pageNo","UTF-8") + "=" + URLEncoder.encode("1", "UTF-8")); /*페이지번호*/
        urlBuilder.append("&" + URLEncoder.encode("numOfRows","UTF-8") + "=" + URLEncoder.encode("10", "UTF-8")); /*한 페이지 결과 수*/
        urlBuilder.append("&" + URLEncoder.encode("startCreateDt","UTF-8") + "=" + URLEncoder.encode("20200310", "UTF-8")); /*검색할 생성일 범위의 시작*/
        urlBuilder.append("&" + URLEncoder.encode("endCreateDt","UTF-8") + "=" + URLEncoder.encode("20200315", "UTF-8")); /*검색할 생성일 범위의 종료*/
        
        URL url = new URL(urlBuilder.toString());
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Content-type", "application/json");
        System.out.println("Response code: " + conn.getResponseCode());
        
        BufferedReader rd;
        if(conn.getResponseCode() >= 200 && conn.getResponseCode() <= 300) {
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        } else {
            rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
        }
        
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = rd.readLine()) != null) {
            sb.append(line);
        }
        
        rd.close();
        conn.disconnect();
        Covid19VO covid19 = om.readValue(sb.toString(), Covid19VO.class);
        System.out.println(sb.toString());
        
        return covid19;
		
	} 

	
	

}

